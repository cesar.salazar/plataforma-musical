var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
    var models = require('./../models');
    models.sequelize.sync().then(() =>{
       console.log("Se ha sincronizado los modelos"); 
       res.send('Se ha sincronizado la bd');
    }).catch(err =>{
        console.log(err,"Hubo un error");
        res.send('No se pudo sincronizar la bd ');
    });
});

module.exports = router;

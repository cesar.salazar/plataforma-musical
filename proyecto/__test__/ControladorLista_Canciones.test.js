const listaC = require('../controladores/ControladorCancion');
const listaCanciones = new listaC();
const mockLC = require('../__mocks__/ControladorLista_Canciones.mock');

describe('Agregar lista de canciones', () =>{
    it('Proceso normal', () => {
        expect(mockLC.agregarLista()).toEqual([{
            nombre: "Aviones",
            genero: "Balada",
            colaborador: null,
            direccion: "public/canciones/aviones.mp3",
            reproducciones:3
        }]);
    });
});


describe('Eliminar lista de canciones', () =>{
    it('Proceso normal', () => {
        expect(mockLC.eliminarLista()).toEqual(["Se ha eliminado la lista correctamente"]);
    });
});

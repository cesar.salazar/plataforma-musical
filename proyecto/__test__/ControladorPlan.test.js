const planC = require('../controladores/ControladorPlan');
const plan = new planC();
const mocksPl = require('../__mocks__/ControladorPlan.mock');

describe('Mock of Controller Plan',()=>{
    test('Test of save',()=>{
        expect(mocksPl.guardar_plan()).toEqual([{
        costo:"25,2",
       nombre_plan:"gold"
        }]);
    });
     test('Datos Nulos',()=>{
        expect(mocksPl.guardar_null()).toBeNull();
    });
});

describe('Mock of Vizualizar Plan',()=>{
    test('Test of save',()=>{
        expect(mocksPl.ver_plan()).toEqual([{
        costo:"34,3",
        nombre_plan:"golden",
         id_persona:2
        }]);
    });
     test('Datos Nulos',()=>{
        expect(mocksPl.ver_plan_null()).toBeNull();
    });
});
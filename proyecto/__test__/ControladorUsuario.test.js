const mockU = require('../__mocks__/ControladorUsuario.mock');


describe('Operaciones usuario', () =>{
    it('Iniciar sesion', () => {
        expect(mockU.inicioSesion()).toEqual([{
            title: 'Aureo',
            fragmentos: "PaginaPrincipal",
            info: { artista: "Casv"},
            normal: true,
        }]);
    });
    it('Cerrar sesion', () => {
        expect(mockU.cierreSesion()).toEqual([{}]);
    });
});
const albumC = require('../controladores/ControladorAlbum');
const album = new albumC();
const mocksA = require('../__mocks__/ControladorAlbum.mock');

describe('Crear Album', () =>{
    it('Proceso normal', () => {
        expect(mocksA.crear_album()).toEqual([{
            nom_album: "this town",
            portada: "public/logob.png"
        }]);
    });
    it('Crear Album Null',()=>{
        expect(mocksA.crea_album_null()).toBeNull();
    });
});
describe('Eliminar Album', () =>{
    it('Proceso normal', () => {
        expect( mocksA.eliminarAlbum()).toEqual(["Se ha eliminado la lista correctamente"]);
    });
});

describe('Vizualizar Album', () =>{
    it('Ver Album', ()=> {
        expect(mocksA.ver_album()).toEqual([{
            nom_album: "aero",
            portada:"portada"
         }]);
    });
    it('Ver Album Null',()=>{
        expect(mocksA.ver_album_null()).toBeNull();
    });
});

describe('Buscar Album', () =>{
    it('Normal Album', ()=> {
        expect(mocksA.buscar_album()).toEqual([{
            nom_album: "aero"
        }]);
    });
    it('Buscar Album Null',()=>{
        expect(mocksA.buscar_null()).toBeNull();
    });
});


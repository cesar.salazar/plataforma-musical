const listaRe = require('../controladores/ControladorLista_reproduccion');
const listaR = new listaRe();
const mocks = require('../_mocks_/ControladorListaRep.mock');

describe('Mock of ControllerListaRep',()=>{
    test('Test of crear Listar',()=>{
        expect(mocks.crear_listar()).toEqual({
             nom_lista : "lista1",
            descripcion : "Mi lista",
              caratula : "public/logob.png"
        });

    });
    test('Datos Nulos',()=>{
       expect(mocks.lista_null()).toBeNull();
    });

});

describe('Eliminar lista de canciones', () =>{
    it('Proceso normal', () => {
        expect(mocks.eliminarLista()).toEqual(["Se ha eliminado la lista correctamente"]);
    });
});


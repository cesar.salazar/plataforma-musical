const personaC = require('../controladores/ControladorPersona');
const persona = new personaC();
const mocksPer = require('../__mocks__/ControladorPersona.mock');


describe('Mock of ControllerPersona',()=>{
    test('Test Normal',()=>{
        expect(mocksPer.guardar_data_persona()).toEqual([{
            apellidos:"rivas",
            nombres:"yanela",
            telefono:"9182838"
        }]);
    });
});
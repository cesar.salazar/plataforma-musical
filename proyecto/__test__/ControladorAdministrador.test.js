const administrador = require('../controladores/ControladorAdministardor');
const admin = new administrador();
const mockdAdmin =require('../__mocks__/ControladorAdministrador.mock');

describe('Listar Usuarios Normal',()=>{
    test('Normal',()=>{
        expect(mockdAdmin.listar_usuarios()).toEqual([{
            id:1,
            correo: "admin@admin.com",
            clave: "******"
    }]);
    });
    test('Datos Nulos',()=>{
        expect(mockdAdmin.null_usuarios()).toBeNull();
    });
});
describe('Listar Canciones Normal',()=>{
    test('Normal',()=>{
        expect(mockdAdmin.listar_canciones()).toEqual([{
            nombre: "Aviones",
            genero: "Balada",
            colaborador: null,
            direccion: "public/canciones/aviones.mp3",
            reproducciones:3
        }]);
    });
    test('Datos Nulos',()=>{
        expect(mockdAdmin.null_canciones()).toBeNull();
    });
});
const cancionC = require('../controladores/ControladorCancion');
const cancion = new cancionC();
const mockC = require('../__mocks__/ControladorCancion.mock');

describe('Agregar canciones', () =>{
    it('Proceso normal', () => {
        expect(mockC.agregarCancion()).toEqual([{
            nombre: "Aviones",
            genero: "Balada",
            colaborador: null,
            direccion: "public/canciones/aviones.mp3",
            reproducciones:3
        }]);
    });
});

describe('Visualizar canciones', () =>{
    it('Nuevas canciones', () => {
        expect(mockC.visualizarCanciones()).toEqual([{
            cancion:{
                nombre: "Aviones",
                genero: "Balada",
                artista: "Casv",
                direccion: "public/canciones/aviones.mp3",
                portada:"public/portadas/2cae5258-9166-4546-b592-56a3eaf99a10aviones.jpg",
                nombre_album:"Aeropuerto"
            }
        }]);
    });

    it('Top10', () => {
        expect(mockC.visualizartop10()).toEqual([{
            canciones:{
                cancion1:{
                    nombre: "A"
                },
                cancion2:{
                    nombre: "B"
                },
                cancion3:{
                    nombre: "C"
                },
                cancion4:{
                    nombre: "D"
                },
                cancion5:{
                    nombre: "E"
                },
                cancion6:{
                    nombre: "F"
                },
                cancion7:{
                    nombre: "G"
                },
                cancion8:{
                    nombre: "H"
                },
                cancion9:{
                    nombre: "I"
                },
                cancion10:{
                    nombre: "J"
                },
            }
        }]);
    });

    it('Canciones', () => {
        expect(mockC.visualizarCancionesUsuario()).toEqual([{
            cancion:{
                nombre: "Aviones",
                genero: "Balada",
                artista: "Casv",
                direccion: "public/canciones/aviones.mp3",
                portada:"public/portadas/2cae5258-9166-4546-b592-56a3eaf99a10aviones.jpg",
                nombre_album:"Aeropuerto"
            }
        }]);
    });

    it('Subir canción', () => {
        expect(mockC.subirCancion()).toEqual([{
            nombre: "Aviones",
            genero: "Balada",
            colaborador: null,
            nombre_album:"Aeropuerto"
        }]);
    });
});
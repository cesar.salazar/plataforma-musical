const data_crear = {
    nom_album: "this town",
    portada: "public/logob.png"
};

const data_album={
    nom_album: "aero",
    portada:"portada"
}
const data_buscar={
    nom_album: "aero"
}
const notificacion="Se ha eliminado la lista correctamente";
var eliminarAlbum = jest.fn().mockReturnValue([notificacion]);
var crear_album = jest.fn().mockReturnValue([data_crear]);

var crea_album_null= jest.fn().mockReturnValue(null);
var ver_album=jest.fn().mockReturnValue([data_album]);
var ver_album_null=jest.fn().mockReturnValue(null);
var buscar_album=jest.fn().mockReturnValue([data_buscar]);
var buscar_null=jest.fn().mockReturnValue(null);
module.exports = {crear_album,crea_album_null,ver_album,ver_album_null,eliminarAlbum,buscar_album,buscar_null};
const data_usuarios = {
    id:1,
    correo: "admin@admin.com",
    clave: "******"
}
const data_canciones={
    nombre: "Aviones",
    genero: "Balada",
    colaborador: null,
    direccion: "public/canciones/aviones.mp3",
    reproducciones:3

}
var listar_usuarios= jest.fn().mockReturnValue([data_usuarios]);
var null_usuarios=jest.fn().mockReturnValue(null);

var listar_canciones=jest.fn().mockReturnValue([data_canciones]);
var null_canciones=jest.fn().mockReturnValue(null);
module.exports = {listar_usuarios,null_usuarios,listar_canciones,null_canciones}

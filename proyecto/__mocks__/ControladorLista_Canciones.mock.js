const agr_lista = {
    nombre: "Aviones",
    genero: "Balada",
    colaborador: null,
    direccion: "public/canciones/aviones.mp3",
    reproducciones:3
};
const notificacion = "Se ha eliminado la lista correctamente";

var agregarLista = jest.fn().mockReturnValue([agr_lista]);
var eliminarLista = jest.fn().mockReturnValue([notificacion]);

module.exports = {agregarLista,eliminarLista};
const agr_cancion = {
    nombre: "Aviones",
    genero: "Balada",
    colaborador: null,
    direccion: "public/canciones/aviones.mp3",
    reproducciones:3
};

const vis_nuev_cancion = {
    cancion:{
        nombre: "Aviones",
        genero: "Balada",
        artista: "Casv",
        direccion: "public/canciones/aviones.mp3",
        portada:"public/portadas/2cae5258-9166-4546-b592-56a3eaf99a10aviones.jpg",
        nombre_album:"Aeropuerto"
    }
};

const vis_top_10 = {
    canciones:{
        cancion1:{
            nombre: "A"
        },
        cancion2:{
            nombre: "B"
        },
        cancion3:{
            nombre: "C"
        },
        cancion4:{
            nombre: "D"
        },
        cancion5:{
            nombre: "E"
        },
        cancion6:{
            nombre: "F"
        },
        cancion7:{
            nombre: "G"
        },
        cancion8:{
            nombre: "H"
        },
        cancion9:{
            nombre: "I"
        },
        cancion10:{
            nombre: "J"
        },
    }
}

const vis_cancion = {
    cancion:{
        nombre: "Aviones",
        genero: "Balada",
        artista: "Casv",
        direccion: "public/canciones/aviones.mp3",
        portada:"public/portadas/2cae5258-9166-4546-b592-56a3eaf99a10aviones.jpg",
        nombre_album:"Aeropuerto"
    }
};


const subir_cancion = {
    nombre: "Aviones",
    genero: "Balada",
    colaborador: null,
    nombre_album:"Aeropuerto"
};


var agregarCancion = jest.fn().mockReturnValue([agr_cancion]);
var visualizarCanciones = jest.fn().mockReturnValue([vis_nuev_cancion]);
var visualizartop10 = jest.fn().mockReturnValue([vis_top_10]);
var visualizarCancionesUsuario = jest.fn().mockReturnValue([vis_cancion]);
var subirCancion= jest.fn().mockReturnValue([subir_cancion]);
module.exports = {agregarCancion,visualizarCanciones,visualizartop10, visualizarCancionesUsuario, subirCancion};
const data = {
    nom_lista : "lista1",
    descripcion : "Mi lista",
    caratula : "public/logob.png"
}
const notificacion = "Se ha eliminado la lista correctamente";
var crear_listar = jest.fn().mockReturnValue(data);
var eliminarLista = jest.fn().mockReturnValue([notificacion]);
var lista_null = jest.fn().mockReturnValue(null);
module.exports = {crear_listar,eliminarLista,lista_null};
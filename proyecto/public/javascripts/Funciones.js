/**
 * @function cambiarFecha
 * @description Obtiene la fecha de nacimiento del usuario y la transforma al formato que guarda la base de datos
 * @param {date} fecha_recibida Es la fecha que se obtiene  
 * @returns {fecha} Devuelve la fecha transformada
 */
function cambiarFecha(fecha_recibida) {
    var d = new Date(fecha_recibida);
    var mes = contar_cero((d.getMonth() + 1), 2);
    var dias = contar_cero(d.getDate(), 2);
    var fecha = (dias + "-" + mes + "-" + d.getFullYear());
    $("#fecha").val(fecha);
}
$(document).ready(function () {
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = now.getFullYear() + "-" + (month) + "-" + (day);
    $("#fechas").val(today);
});


/**
 * @function contar_cero
 * @description Trabaja junto a la función cambiarFechaparaGuardar, para transformar los meses y días al formato permitido por mysql
 * @param {int} texto Es el número de la fecha obtenida
 * @param {int} nro_cero Es el número de ceros que se necesita
 * @returns {String} Devuelve una cadena con los números de la fecha con el formato correspondiente
 */
function contar_cero(texto, nro_cero) {
    texto = texto + "";
    var longi = texto.length;
    var aux = nro_cero - longi;
    var cadena = "";
    if (aux < nro_cero) {
        for (var i = longi; i < nro_cero; i++) {
            cadena += '0';
        }
        cadena += texto;
        return cadena;
    } else {
        return texto;
    }
}

/**
 * @function existeFecha
 * @description Valida que la fecha ingresada exista
 * @param {Date} fecha
 * @returns {Boolean}
 */
function existeFecha(fecha) {
    var fechaf = fecha.split("/");
    var day = fechaf[0];
    var month = fechaf[1];
    var year = fechaf[2];
    var date = new Date(year, month, '0');
    if ((day - 0) > (date.getDate() - 0)) {
        return false;
    }
    return true;
}

/**
 * @function fecha
 * @description Obtiene la fecha y verifica si es una fecha valida
 * @type type
 */
function  fecha(m) {
    var fecha = m;
    if (existeFecha(fecha)) {
        return true;
    } else {
        return false;
    }

}

/**
 * @function Validacion
 * @description Permite validar todos los datos de los formularios
 * @returns {undefined} 
 */
$(function () {
    $.validator.addMethod("sololetras", function (value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
    }, "Solo letras");

    $.validator.addMethod("fechas", function (value, element) {
        return this.optional(element) || fecha(value);
    }, " Fecha no válida");
     $.validator.addMethod("tipo", function (value, element) {
            return this.optional(element) || extractCardInfo(value);
        }, "Tarjeta no valida");
    $.validator.addMethod("correo", function (value, element) {
        return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Correo eléctronico incorrecto");
    

    $.validator.addMethod("fecha_card", function (value, element) {
        return this.optional(element) || /^2017|2018|2019|2020|2021|2022|2023|2024|2025|2026|2027|2028|2029|2030|2031$/.test(value);
    }, "Número de teléfono no válido");

    $("#form-login").validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        ignore: "",
        rules: {
            nombres: {
                required: true,
                maxlength: 20,
                minlength: 4,
                sololetras: true

            },
            apellidos: {
                required: true,
                maxlength: 20,
                minlength: 5,
                sololetras: true

            },

            correo: {
                required: true,
                correo: true

            },
            clave: {
                required: true,

            },

            fecha: {
                required: true,
                fechas: true


            }, celular: {
                required: true,
                number: true,
                maxlength: 10,
                minlength: 10


            },
            nombres_artista: {
                required: true,
                maxlength: 20,
                minlength: 2

            }
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
            $(e).remove();

        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    $("#formulario").validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        ignore: "",
        rules: {
            foto: {
                required: true
            }
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
            $(e).remove();

        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    $("#formulario_listareproduccion").validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        ignore: "",
        rules: {
            nombre: {
                required: true,
                maxlength: 30,
                minlength: 3,
                sololetras: true

            },
            descripcion: {
                required: true,
                maxlength: 255,
                minlength: 5
            }
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
            $(e).remove();

        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    $("#formulario_album").validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        ignore: "",
        rules: {
            nombre: {
                required: true,
                maxlength: 40,
                minlength: 1,
                sololetras: true
            }
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
            $(e).remove();

        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    $("#formulario_cancion").validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        ignore: "",
        rules: {
            nombres: {
                required: true,
                maxlength: 40,
                minlength: 1
            },
            genero: {
                required: true,
                maxlength: 10,
                minlength: 3,
                sololetras: true
            },
            colaboradores: {
                maxlength: 50
            },
            cancion: {
                required: true
            }

        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
            $(e).remove();

        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    $("#formularios_tar").validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        ignore: "",
        rules: {
            card: {
                required: true,
                number: true,
                maxlength: 19,
                minlength: 19,
                tipo:true


            }, year: {
                required: true,
                number:true,
                maxlength: 4,
                minlength: 4,
                fecha_card:true
            }, cvv: {
                required: true,
                number: true
            }

        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
            $(e).remove();

        },
        submitHandler: function (form) {
            form.submit();
        }
    });

});


/**
 * @function reproductor
 * @description Permite reproducir la canción seleccionada
 * @param {String} direccion Obtiene la direccion de la canción
 * @param {String} portada Obtiene la portada de la canción
 * @param {String} cancion Obtiene el nombre de la canción 
 * @param {String} artista Obtiene el nombre del artista de la canción
 * @param {String} external_cancion obtiene el external id de la canción
 */
function reproductor(direccion, portada, cancion, artista, external_cancion) {
    reproduccion(external_cancion);
    $('#portada').attr('src', '/portadas/' + portada);
    $('#player').attr('src', '/canciones/' + direccion);
    var titulo = artista + '-' + cancion;
    $('#titulo').val(titulo);
    document.getElementById('player').play();
}
;

/**
 * @function reproduccion
 * @description Cuenta las veces en las que se va reproduciendo la canción
 * @param {String} external Obtiene el external id de la canción 
 * @returns {undefined}
 */
function reproduccion(external) {
    var models = require('./../models/');
    var cancion = models.cancion;
    cancion.findOne({where: {external_id: external}}).then(datos => {
        var reproduccion = datos.reproducciones;
        reproduccion = reproduccion + 1;
        var Datos = datos;
        Datos.reproducciones = reproduccion;
        Datos.save().then(function (result) {
            return;
        });
    });
}
;
function extractCardInfo(text) {
    var cards = {
        "American Express": /\b3[47][0-9]{13}\b/,
        "Mastercard": /\b5[1-5][0-9]{14}\b/,
        "Visa": /\b4[0-9]{12}(?:[0-9]{3})?\b/
    };

    for (var card in cards) {
        if (cards[card].test(text)) {
            var arr = text.match(cards[card]);
            return true;
        }else{
            return false;
        }
    }

    
}

